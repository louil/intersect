
#include "intersect_functions.h"

static const size_t DEFAULT_SIZE = 10;
static struct h_llist *h_llist_create(const char *key, size_t value);
static void h_llist_destroy(struct h_llist *list);
static size_t hash_func(const char *key, size_t capacity);
static void hash_recalculate(hash *h);

//Derived from dsprimm
void print(char *word){
    printf("%s\n", word);
}

//from the hash program in class
uint64_t wang_hash(uint64_t key){
	key = (~key) + (key << 21);
	key = key ^ (key >> 24);
	key = (key + (key << 3)) + (key << 8);
	key = key ^ (key >> 14);
	key = (key + (key << 2)) + (key << 4);
	key = key ^ (key >> 28);
	key = key + (key << 31);
	return key;
}

//from the hash program in class
hash *__hash_create(size_t capacity){
	hash *h = malloc(sizeof(*h));
	if(!h){
		return NULL;
	}

	h->data = calloc(capacity, sizeof(*h->data));

	if(!h->data){
		free(h);
		return NULL;
	}

	h->capacity = capacity;
	h->item_count = 0;

	return h;
}
//from the hash program in class
hash *hash_create(void){
	return __hash_create(DEFAULT_SIZE);
}

//from the list program in class
static struct h_llist *h_llist_create(const char *key, size_t value){
	struct h_llist *node = malloc(sizeof(*node));

	if(!node){
		return NULL;
	}

	node->key = strdup(key);
	if(!node->key){
		free(node);
		return NULL;
	}
	node->value = value;
	node->next = NULL;

	return node;
}

//from the list program in class
static void h_llist_destroy(struct h_llist *list){
	while(list){
		struct h_llist *tmp = list->next;
		free(list->key);
		free(list);
		list = tmp;
	}
}

//from the hash program in class
void hash_destroy(hash *h){
	if(h){
		for(size_t n = 0; n < h->capacity; ++n){
			h_llist_destroy(h->data[n]);
		}
	}

	free(h->data);
	free(h);	
}

//from the hash program in class with some slight alterations
static size_t hash_func(const char *key, size_t capacity){
	uint64_t buf = 0;
	strncpy((char *)(&buf), key, sizeof(buf));

	return wang_hash(buf) % capacity;
}	

//from the hash program in class
void hash_insert(hash *h, const char *key, size_t value){
	if(!h || !key){
		return;
	}

	hash_recalculate(h);

	size_t idx = hash_func(key, h->capacity);

	struct h_llist *tmp = h->data[idx];

	while(tmp){
		if(strcmp(tmp->key, key) == 0){
			tmp->value = value;
			return;
		}
		tmp = tmp->next;
	}

	struct h_llist *new = h_llist_create(key, value);
	if(!new){
		return;
	}
	
	new->next = h->data[idx];

	h->data[idx] = new;

	h->item_count += 1;
}

//from the hash program in class
size_t hash_fetch(hash *h, const char *key){
	if(!h || !key){
		return 0;
	}

	size_t idx = hash_func(key, h->capacity);

	struct h_llist *tmp = h->data[idx];

	while(tmp){
		if(strcmp(tmp->key, key) == 0) {
			return tmp->value;
		}

		tmp = tmp->next;
	}

	return 0;
}

//from the hash program in class
static void hash_recalculate(hash *h){
	if(!h){
		return;
	}

	if(h->item_count < 0.70 * h->capacity){
		return;
	}

	hash *cpy = __hash_create(h->capacity * 2);
	if(!cpy){
		return;
	}

	for(size_t n = 0; n < h->capacity;  ++n){
		struct h_llist *tmp = h->data[n];
		while(tmp) {
			hash_insert(cpy, tmp->key, tmp->value);
			tmp = tmp->next;
		}
	}
	for(size_t n = 0; n < h->capacity; ++n) {
		h_llist_destroy(h->data[n]);
	}
	free(h->data);

	h->capacity = cpy->capacity;
	h->item_count = cpy->item_count;
	h->data = cpy->data;

	free(cpy);
}

struct h_llist *hash_to_ll(hash *h){
	struct h_llist *head = NULL;

	for (size_t n = 0; n < h->capacity; ++n){
		if(!h->data[n]) {
			continue;
		}

		struct h_llist *tail = h->data[n];
		while(tail->next) {
			tail = tail->next;
		}

		tail->next = head;
		head = h->data[n];
	}

	memset(h->data, '\0', (h->capacity * sizeof(*h->data)));

	return head;
}

//Code derived from dsprimm
void fill_bst(bst *sorted, hash *h, size_t file_count){
	if(!h){
        return;
    }

	for(size_t n = 0; n < h->capacity; ++n){
    	struct h_llist *tmp = h->data[n];
    	while(tmp){
        	if(tmp->value == file_count - 1){
            	bst_insert(sorted, tmp->key);
        	}
        	tmp = tmp->next;
    	}
	}
}

//Taken from class tree exercise
bst *bst_create(void){
	bst *b = malloc(sizeof(*b));
	if(b){
		b->root = NULL;
	}

	return b;
}

//Taken from class tree exercise
void bst_destroy(bst *b){
	if(!b){
		return;
	}
	tree_destroy(b->root);
	free(b);
}

//Taken from class tree exercise
static bool tree_insert(struct tree **t, char *data){
	if(!*t){
		*t = tree_create(data);
		return *t;
	}
	struct tree *node = *t;

	if(strcmp(data, node->data) > 0){
		return tree_insert(&node->right, data);
	} 
	else{
		return tree_insert(&node->left, data);
	}
}

//Taken from class tree exercise
bool bst_insert(bst *b, char *data){
	if(!b){
		return false;
	}
	return tree_insert(&b->root, data);
}

//Taken from class tree exercise
struct tree *tree_create(char *data){
	struct tree *t = malloc(sizeof(*t));
	if(t){
		t->data = data;
		t->left = NULL;
		t->right = NULL;
	}

	return t;
}

//Taken from class tree exercise
void tree_destroy(struct tree *t){
	if(!t){
		return;
	}

	tree_destroy(t->left);
	tree_destroy(t->right);
	free(t);
}

//Taken from class tree exercise
void tree_inorder(struct tree *t, void (*func)(char *)){
	if(!t){
		return;
	}

	tree_inorder(t->left, func);
	func(t->data);
	tree_inorder(t->right, func);
}