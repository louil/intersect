CFLAGS+=-std=c11
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline

intersect: intersect.o intersect_functions.o

man:
	sudo cp intersect.1 /usr/share/man/man1/

.PHONY: clean debug

clean:
	-rm *.o

debug: CFLAGS+=-g
debug: intersect

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: intersect
