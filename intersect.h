
#ifndef INTERSECT_H
 #define INTERSECT_H
 
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//Taken from the hash program
struct h_llist {
	char *key;
	size_t value;
	struct h_llist *next;
};

//Taken from the hash program
typedef struct {
	size_t item_count;
	size_t capacity;
	struct h_llist **data;
} hash;

//Taken from class tree exercise
struct tree {
	char * data;
	struct tree *left;
	struct tree *right;
};

//Taken from class tree exercise
typedef struct {
	struct tree *root;
} bst;

//Taken from class tree exercise
bst *bst_create(void);
void bst_destroy(bst *b);
struct tree *tree_create(char * data);
void tree_destroy(struct tree *t);
void tree_inorder(struct tree *t, void (*func)(char *));
bool bst_insert(bst *b, char *data);

//Derived from dsprimm
void fill_bst(bst *sorted, hash *h, size_t file_count);

//Taken from the hash program
hash *hash_create(void);
void hash_destroy(hash *h);
void hash_insert(hash *h, const char *key, size_t value);
size_t hash_fetch(hash *h, const char *key);
struct h_llist *hash_to_ll(hash *h);

#endif