
#include "intersect.h"
#include "intersect_functions.h"

const size_t BUFF_SZ = 256;

int main(int argc, char *argv[]){
	if(argc == 1){
		fprintf(stderr, "There were no files\n");
		return 1;
	}

	size_t file_count = 1;

	hash *hashy = hash_create();

	char *buffer = malloc(BUFF_SZ * sizeof(buffer));

	//While there is a file open that file at that position in argv
	while(file_count < (size_t)argc){
		FILE *fp = fopen(argv[file_count], "r");

		//If the file doesn't open then print out the error, free the buff and return.
		if(!fp){
			perror("The file could not be opened\n");
			free(buffer);
			return 2;
			//exit(1);
		}

		//While the file does not reach the end of file grab a certain amount for a buffer
		//then create a token and while that token is not NULL fetch from the hash table
		//and if it is equal to the file_count then to insert into the has with the current file_count
		while(!feof(fp)){
			if(fgets(buffer, BUFF_SZ, fp) != NULL){
				char *token = strtok(buffer, " \t\n\f\v\r");
				while(token != NULL){
					if(hash_fetch(hashy, token) == file_count - 1){
						hash_insert(hashy, token, file_count);
					}
					token = strtok(NULL, " \t\n\f\v\r");
				}
			}
		}	

		fclose(fp);
		++file_count;
	}

	//Create bst combine, then fill it, and then place it in order
	bst *combine = bst_create();
	fill_bst(combine, hashy, file_count);
	tree_inorder(combine->root, print);
	
	//Free the buffer, destroy the hash, and then destroy the bst
	free(buffer);
	hash_destroy(hashy);
	bst_destroy(combine);
}


